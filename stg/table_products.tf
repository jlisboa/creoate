module "Product" {
  source         = "../modules/dynamo_db_tables"
  name           = "Product"
  hash_key       = "id"
  hash_key_type  = "S"
  range_key      = "name"
  range_key_type = "S"
  environment    = "dev"

  attributes = [
    {
      name = "category"
      type = "S"
    }
  ]

  global_secondary_indexes = [
    {
      name               = "CategoryIndex"
      hash_key           = "name"
      range_key          = "category"
      projection_type    = "INCLUDE"
      non_key_attributes = ["id"]
    }
  ]

  tags = {
    other = "teste"
  }
}