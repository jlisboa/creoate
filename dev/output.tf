output "table_arn_retailers" {
  value = module.Retailers.dynamodb_table_arn
}

output "table_id_retailers" {
  value = module.Retailers.dynamodb_table_id
}

output "table_arn_product" {
  value = module.Product.dynamodb_table_arn
}

output "table_id_product" {
  value = module.Product.dynamodb_table_id
}