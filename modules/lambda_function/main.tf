resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "dynamodb:BatchWriteItem",
                "dynamodb:PutItem",
                "dynamodb:DescribeTable",
                "dynamodb:DeleteItem",
                "dynamodb:GetItem",
                "dynamodb:Query",
                "dynamodb:UpdateItem",
                "dynamodb:UpdateGlobalTable",
                "dynamodb:UpdateTable",
                "dynamodb:GetRecords"
            ],
            "Resource": [
                "arn:aws:dynamodb:*:147187214478:table/${var.retailers_table_id}",
                "arn:aws:dynamodb:*:147187214478:table/${var.product_table_id}",
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "dynamodb:ListGlobalTables",
                "dynamodb:ListTables"
            ],
            "Resource": [
                "arn:aws:dynamodb:*:147187214478:table/${var.retailers_table_id}",
                "arn:aws:dynamodb:*:147187214478:table/${var.product_table_id}",
            ]
        }
    ]
}
EOF
}

resource "aws_lambda_function" "test_lambda" {
  filename      = var.filename
  function_name = "${var.function_name}-${var.environment}"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = var.handler

  source_code_hash = filebase64sha256(var.filename)

  runtime = var.runtime

  environment {
    variables = merge(
      var.variables,
      {},
    )
  }
}