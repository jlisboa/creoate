variable "filename" {
  description = "Zip filename"
  type        = string
}

variable "function_name" {
  description = "Lamdba function filename"
  type        = string
}

variable "environment" {
  description = "Environment to deploy"
  type        = string
}

variable "handler" {
  description = "Lambda function handler"
  type        = string
}

variable "runtime" {
  description = "Runtime to run lambda function"

}
variable "variables" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "retailers_table_id" {
  description = "Table id for retailers"
}

variable "product_table_id" {
  description = "Table id for products"
}