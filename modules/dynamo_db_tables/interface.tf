variable "name" {
  description = "Table name"
  type        = string
}

variable "billing_mode" {
  description = "Billing mode type"
  type        = string
  default     = "PAY_PER_REQUEST"

  validation {
    condition     = contains(["PROVISIONED", "PAY_PER_REQUEST"], var.billing_mode)
    error_message = "The billing mode value must be PROVISIONED or PAY_PER_REQUEST."
  }
}

variable "stream_enabled" {
  description = "Stream"
  type        = bool
  default     = false
}

variable "read_capacity" {
  description = "Read Capacity"
  type        = number
  default     = null
}

variable "write_capacity" {
  description = "Write Capacity"
  type        = number
  default     = null
}

variable "hash_key" {
  description = "Hash Key value"
  type        = string
}

variable "hash_key_type" {
  description = "Hash Key Type"

  validation {
    condition     = contains(["S", "N", "B"], var.hash_key_type)
    error_message = "Hash Key Type must be a scalar. S - String, N - Number, B - Binary."
  }
}

variable "range_key" {
  description = "Range Key value"
  type        = string
  default     = null
}

variable "range_key_type" {
  description = "Range Key Type"
  default     = null

  validation {
    condition     = var.range_key_type == null ? true : contains(["S", "N", "B"], var.range_key_type)
    error_message = "Hash Key Type must be a scalar. S - String, N - Number, B - Binary."
  }
}

variable "attributes" {
  description = "List of nested attribute definitions. Only required for hash_key and range_key attributes. Each attribute has two properties: name - (Required) The name of the attribute, type - (Required) Attribute type, which must be a scalar type: S, N, or B for (S)tring, (N)umber or (B)inary data"
  type        = list(map(string))
  default     = []
}

variable "global_secondary_indexes" {
  description = "Describe a GSI for the table; subject to the normal limits on the number of GSIs, projected attributes, etc."
  type        = any
  default     = []
}

variable "local_secondary_indexes" {
  description = "Describe an LSI on the table; these can only be allocated at creation so you cannot change this definition after you have created the resource."
  type        = any
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "environment" {
  description = "Environment Declaration"

  validation {
    condition     = contains(["dev", "tst", "stg", "prod"], var.environment)
    error_message = "Environment must be declared: dev, tst, stg, prod."
  }
}