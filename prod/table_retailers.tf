module "Retailers" {
  source         = "../modules/dynamo_db_tables"
  name           = "Retailers"
  hash_key       = "id"
  hash_key_type  = "S"
  range_key      = "name"
  range_key_type = "S"
  environment    = "dev"
}