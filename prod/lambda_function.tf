module "lambda_eu_west_1" {
  source             = "../modules/lambda_function"
  function_name      = var.function_name
  environment        = var.environment
  handler            = var.handler
  filename           = var.filename
  retailers_table_id = module.Retailers.dynamodb_table_id
  product_table_id   = module.Product.dynamodb_table_id
  runtime            = var.runtime
}

module "lambda_eu_west_2" {
  source             = "../modules/lambda_function"
  function_name      = var.function_name
  environment        = var.environment
  handler            = var.handler
  filename           = var.filename
  retailers_table_id = module.Retailers.dynamodb_table_id
  product_table_id   = module.Product.dynamodb_table_id
  runtime            = var.runtime

  providers = {
    aws = aws.eu-west-2
  }
}

module "lambda_eu_central_1" {
  source             = "../modules/lambda_function"
  function_name      = var.function_name
  environment        = var.environment
  handler            = var.handler
  filename           = "example"
  retailers_table_id = module.Retailers.dynamodb_table_id
  product_table_id   = module.Product.dynamodb_table_id
  runtime            = var.runtime

  providers = {
    aws = aws.eu-central-1
  }
}